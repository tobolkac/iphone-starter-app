//
//  main.m
//  Starter App
//
//  Created by Clay Tobolka on 1/26/14.
//  Copyright (c) 2014 Clay Tobolka. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "tobAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([tobAppDelegate class]));
    }
}
