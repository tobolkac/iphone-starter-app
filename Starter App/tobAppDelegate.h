//
//  tobAppDelegate.h
//  Starter App
//
//  Created by Clay Tobolka on 1/26/14.
//  Copyright (c) 2014 Clay Tobolka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tobAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
